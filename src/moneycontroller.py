#!/usr/local/bin/python3.5

from collections import Counter

class MoneyController:

    def __init__(self):
        self._vending_controller = None
        self._total = 0
        self._inserted_coins = Counter()
        self._returned_coins = Counter()
        return

    def attach_vending_controller(self, vc):
        self._vending_controller = vc
        return

    def insert_coin(self, coin):
        value = dict(
            nickel	= 5,
            dime	= 10,
            quarter	= 25,
            ).get(coin, None)
        if value is None:
            self._returned_coins[coin] += 1
            value = 0
        else:
            self._inserted_coins[coin] += 1
        self._total += value
        self._vending_controller.notify_total_coins(self._total)
        return

    def return_coins(self):
        self._returned_coins.update(self._inserted_coins)
        self._inserted_coins = Counter()
        self._total = 0
        self._vending_controller.notify_total_coins(self._total)
        return

    def get_returned_coins(self):
        rc, self._returned_coins = self._returned_coins, Counter()
        return rc

    def emit_change(self, value):
        # Consume inserted coins.  Change is not supported.
        self._inserted_coins = Counter()
        self._total = 0
        self._vending_controller.notify_total_coins(self._total)
        return

#--#
