#!/usr/local/bin/python3.5

from vendingcontroller import VendingController
from moneycontroller import MoneyController
from productcontroller import ProductController

#---------------------------------------------------------------------#

class VendingMachine:

    def __init__(self):
        self.vc = VendingController()
        self.mc = MoneyController()
        self.pc = ProductController()
        self.vc.attach_money_controller(self.mc)
        self.vc.attach_product_controller(self.pc)
        self.vc.attach_vending_machine(self)
        self.pc.refill_product('cola', 100, 0)
        self.pc.refill_product('chips', 50, 0)
        self.pc.refill_product('candy', 65, 0)
        self._display = '$0.00' #'INSERT COIN'
        self._alert = None
        return

    def peek_inventory(self):
        return self.pc.get_inventory()

    def get_display(self):
        if self._alert is not None:
            alert, self._alert = self._alert, None
            return alert
        if self._display == '$0.00':
            return 'INSERT COIN'
        return self._display

    def display_total(self, pretty):
        self._display = pretty
        return

    def display_alert(self, message):
        self._alert = message
        return

    def insert_coin(self, coin):
        self.mc.insert_coin(coin)
        return

    def return_coins(self):
        self.mc.return_coins()
        return

    def get_returned_coins(self):
        return self.mc.get_returned_coins()

    def refill_product(self, name, price, count):
        self.pc.refill_product(name, price, count)
        return

    def select_product(self, name):
        self.pc.select_product(name)
        return

#--#
