#!/usr/local/bin/python3.5

class VendingController:

    def __init__(self):
        self._vending_machine = None
        self._money_controller = None
        self._product_controller = None
        self._total_coins = 0
        return

    def attach_vending_machine(self, vm):
        self._vending_machine = vm
        return

    def attach_product_controller(self, pc):
        self._product_controller = pc
        pc.attach_vending_controller(self)
        return

    def attach_money_controller(self, mc):
        self._money_controller = mc
        mc.attach_vending_controller(self)
        return

    def notify_total_coins(self, total):
        self._total_coins = total
        pretty = '$%.2f' % (total * 0.01)
        self._vending_machine.display_total(pretty)
        return

    def product_selected(self, name):
        inventory = self._product_controller.get_inventory()
        item = [ row for row in inventory if row.name == name ][0]
        if item.count < 1:
            self._vending_machine.display_alert('SOLD OUT')
            return
        if item.price > self._total_coins:
            self._vending_machine.display_alert(
                'PRICE $%.2f' % (item.price * 0.01))
            return
        self._product_controller.emit_product(name)
        self._money_controller.emit_change(
            self._total_coins - item.price)
        self._vending_machine.display_alert('THANK YOU')
        return

#--#
