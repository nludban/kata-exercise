#!/usr/local/bin/python3.5

from collections import namedtuple

Inventory = namedtuple('Inventory', 'name price count')

class ProductController:

    def __init__(self):
        self._inventory = [ ]
        self._vending_controller = None
        return

    def attach_vending_controller(self, vc):
        self._vending_controller = vc
        return

    def get_inventory(self):
        # Returned inventory is guaranteed ordered by name, and
        # it's only a copy.
        return sorted(self._inventory)

    def refill_product(self, name, price, count):
        assert isinstance(name, str)
        assert isinstance(price, int) and price >= 0
        assert isinstance(count, int) and count >= 0
        for k, row in enumerate(self._inventory):
            if row.name == name:
                assert price == row.price
                self._inventory[k] = row._replace(
                    count=(row.count + count))
                return
        row = Inventory(name, price, count)
        self._inventory.append(row)
        return

    def select_product(self, name):
        for row in self._inventory:
            if row.name == name:
                self._vending_controller.product_selected(name)
                return
        assert False	# Programmer error.

    def emit_product(self, name):
        for k, row in enumerate(self._inventory):
            if row.name == name:
                self._inventory[k] = row._replace(
                    count=(row.count - 1))
                return
        assert False	# Programmer error.

#--#
