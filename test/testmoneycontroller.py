#!/usr/local/bin/python3.5

from moneycontroller import MoneyController

import unittest
import unittest.mock as mock

import unittest.util; unittest.util._PLACEHOLDER_LEN = 999

#---------------------------------------------------------------------#

class TestMoneyController(unittest.TestCase):

    def setUp(self):
        self.mc = MoneyController()
        self.vc = mock.Mock() # VendingController
        self.mc.attach_vending_controller(self.vc)
        return

    def test_insert_nickel(self):
        self.mc.insert_coin('nickel')
        self.vc.notify_total_coins.assert_called_with(5)
        return

    def test_insert_dime(self):
        self.mc.insert_coin('dime')
        self.vc.notify_total_coins.assert_called_with(10)
        return

    def test_insert_quarter(self):
        self.mc.insert_coin('quarter')
        self.vc.notify_total_coins.assert_called_with(25)
        return

    def test_insert_penny(self):
        self.mc.insert_coin('penny')
        self.vc.notify_total_coins.assert_called_with(0)
        return

    def test_coin_total(self):
        self.mc.insert_coin('quarter')
        self.vc.notify_total_coins.assert_called_with(25)
        self.mc.insert_coin('dime')
        self.vc.notify_total_coins.assert_called_with(35)
        self.mc.insert_coin('dime')
        self.vc.notify_total_coins.assert_called_with(45)
        return

    def test_coin_return(self):
        self.mc.insert_coin('quarter')
        self.mc.insert_coin('dime')
        self.mc.insert_coin('dime')
        self.mc.return_coins()
        self.assertEqual(self.mc.get_returned_coins(),
                         dict(quarter=1, dime=2))
        return

    def test_coin_rejects(self):
        self.mc.insert_coin('quarter')
        self.mc.insert_coin('dime')
        self.mc.insert_coin('dime')
        self.mc.insert_coin('penny')
        self.vc.notify_total_coins.assert_called_with(45)
        self.mc.return_coins()
        self.vc.notify_total_coins.assert_called_with(0)
        self.assertEqual(self.mc.get_returned_coins(),
                         dict(quarter=1, dime=2, penny=1))
        return

#--#
