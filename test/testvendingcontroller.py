#!/usr/local/bin/python3.5

from vendingcontroller import VendingController

from moneycontroller import MoneyController
from productcontroller import ProductController

import unittest
import unittest.mock as mock

import unittest.util; unittest.util._PLACEHOLDER_LEN = 999

#---------------------------------------------------------------------#

class TestVendingController(unittest.TestCase):

    def setUp(self):
        self.vc = VendingController()
        self.mc = MoneyController()
        self.pc = ProductController()
        self.vc.attach_money_controller(self.mc)
        self.vc.attach_product_controller(self.pc)
        self.vm = mock.Mock()	# VendingMachine
        self.vc.attach_vending_machine(self.vm)
        self.pc.refill_product('cola', 100, 0) # Sold out.
        self.pc.refill_product('chips', 50, 4)
        self.pc.refill_product('candy', 65, 2)
        return

    def test_attaching_controllers(self):
        # Dummy to test set up.
        return

    def test_insert_coins_display_total(self):
        self.mc.insert_coin('dime')
        self.vm.display_total.assert_called_with('$0.10')
        self.mc.insert_coin('dime')
        self.vm.display_total.assert_called_with('$0.20')
        for k in range(100):
            self.mc.insert_coin('quarter')
        self.vm.display_total.assert_called_with('$25.20')
        return

    def test_return_coins_display_total(self):
        # Verify edge case (no coins) by inserting and returning
        # in order to trigger a display_total().
        self.mc.insert_coin('nickel')
        self.mc.insert_coin('dime')
        self.mc.insert_coin('quarter')
        self.vm.display_total.assert_called_with('$0.40')
        self.mc.return_coins()
        self.vm.display_total.assert_called_with('$0.00')
        return

    def test_select_product_sold_out(self):
        self.pc.select_product('cola')
        self.vm.display_alert.assert_called_with('SOLD OUT')
        return

    def test_select_product_more_coins(self):
        self.mc.insert_coin('quarter')
        self.mc.insert_coin('quarter')
        self.pc.select_product('candy')
        self.vm.display_alert.assert_called_with('PRICE $0.65')
        return

    def test_select_product_thank_you(self):
        self.mc.insert_coin('quarter')
        self.mc.insert_coin('quarter')
        self.pc.select_product('chips')
        self.vm.display_alert.assert_called_with('THANK YOU')
        return

#--#
