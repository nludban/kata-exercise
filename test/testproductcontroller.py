#!/usr/local/bin/python3.5

from productcontroller import ProductController

import unittest
import unittest.mock as mock

import unittest.util; unittest.util._PLACEHOLDER_LEN = 999

class TestProductController(unittest.TestCase):

    def setUp(self):
        self.pc = ProductController()
        self.vc = mock.Mock() # VendingController
        self.pc.attach_vending_controller(self.vc)
        return

    def test_initial_inventory(self):
        self.assertEqual(self.pc.get_inventory(), [ ])
        return

    def test_add_inventory(self):
        self.pc.refill_product('cola', 100, 1)
        self.pc.refill_product('chips', 50, 1)
        self.pc.refill_product('candy', 65, 1)
        self.assertEqual(
            self.pc.get_inventory(),
            [
                ( 'candy', 65, 1 ),
                ( 'chips', 50, 1 ),
                ( 'cola', 100, 1 ),
            ])
        return

    def test_add_more_inventory(self):
        self.pc.refill_product('cola', 100, 1)
        self.pc.refill_product('chips', 50, 2)
        self.pc.refill_product('candy', 65, 3)
        self.pc.refill_product('cola', 100, 4)
        self.pc.refill_product('chips', 50, 5)
        self.pc.refill_product('candy', 65, 6)
        self.assertEqual(
            self.pc.get_inventory(),
            [
                ( 'candy', 65, 9 ),
                ( 'chips', 50, 7 ),
                ( 'cola', 100, 5 ),
            ])
        return

    def test_select_product(self):
        self.pc.refill_product('cola', 100, 5)
        self.pc.refill_product('chips', 50, 7)
        self.pc.refill_product('candy', 65, 9)

        self.pc.select_product('cola')
        #self.vc.product_selected.assert_called_with('gum')
        self.vc.product_selected.assert_called_with('cola')

        self.pc.select_product('chips')
        self.vc.product_selected.assert_called_with('chips')

        self.pc.select_product('candy')
        self.vc.product_selected.assert_called_with('candy')

        return

    def test_select_sold_out_product(self):
        self.pc.refill_product('cola', 100, 0)

        self.pc.select_product('cola')
        self.vc.product_selected.assert_called_with('cola')

        return

    def test_emit_product(self):
        self.pc.refill_product('cola', 100, 3)
        self.pc.refill_product('chips', 50, 2)
        self.pc.refill_product('candy', 65, 1)

        self.pc.emit_product('cola')
        self.assertEqual(
            self.pc.get_inventory(),
            [
                ( 'candy', 65, 1 ),
                ( 'chips', 50, 2 ),
                ( 'cola', 100, 2 ),
            ])

        # Edge case: last remaining item.
        self.pc.emit_product('candy')
        self.assertEqual(
            self.pc.get_inventory(),
            [
                ( 'candy', 65, 0 ),
                ( 'chips', 50, 2 ),
                ( 'cola', 100, 2 ),
            ])

        return

#--#
