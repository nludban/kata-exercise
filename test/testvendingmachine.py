#!/usr/local/bin/python3.5

from vendingmachine import VendingMachine

import unittest

#---------------------------------------------------------------------#

class TestVendingMachine(unittest.TestCase):

    def setUp(self):
        self.vm = VendingMachine()
        return

    def test_initial_state(self):
        self.assertEqual(
            self.vm.peek_inventory(),
            [
                ( 'candy', 65, 0 ),
                ( 'chips', 50, 0 ),
                ( 'cola', 100, 0 ),
            ])
        self.assertEqual(self.vm.get_display(), 'INSERT COIN')
        return

    def test_accept_coins(self):
        # "When there are no coins inserted ..."
        self.assertEqual(self.vm.get_display(), 'INSERT COIN')

        # "When valid coin inserted, display will be updated"
        self.vm.insert_coin('nickel')
        self.assertEqual(self.vm.get_display(), '$0.05')
        self.vm.insert_coin('dime')
        self.assertEqual(self.vm.get_display(), '$0.15')
        self.vm.insert_coin('quarter')
        self.assertEqual(self.vm.get_display(), '$0.40')

        # "and reject invalid ones (pennies)"
        self.vm.insert_coin('penny')
        self.assertEqual(self.vm.get_display(), '$0.40')

        # "Rejected coins are placed in the coin return"
        self.assertEqual(self.vm.get_returned_coins(),
                         dict(penny=1))

        return

    def test_select_product(self):
        self.vm.refill_product('chips', 50, 5)

        # "Press button, enough money, product + THANK YOU"
        self.vm.insert_coin('quarter')
        self.vm.insert_coin('quarter')
        self.vm.select_product('chips')
        self.assertEqual(self.vm.get_display(),
                         'THANK YOU')

        # "Check again, displays ``INSERT COIN''"
        self.assertEqual(self.vm.get_display(),
                         'INSERT COIN')

        # "Not enough money, PRICE $$$"
        self.vm.select_product('chips')
        self.assertEqual(self.vm.get_display(),
                         'PRICE $0.50')

        # "Check again [no coins], displays ``INSERT COIN''"
        self.assertEqual(self.vm.get_display(),
                         'INSERT COIN')

        # "Not enough money, PRICE $$$"
        self.vm.insert_coin('quarter')
        self.vm.select_product('chips')
        self.assertEqual(self.vm.get_display(),
                         'PRICE $0.50')

        # "Check again [some coins], displays total"
        self.assertEqual(self.vm.get_display(),
                         '$0.25')
        

        return

    #def test_make_change(self):
    #   Not supporting change.

    def test_return_coins(self):

        # Edge case: no coins inserted.
        self.vm.return_coins()
        self.assertEqual(self.vm.get_returned_coins(), { })

        # "Return coins button pressed"
        self.vm.insert_coin('nickel')
        self.vm.insert_coin('dime')
        self.vm.insert_coin('dime')
        self.vm.insert_coin('quarter')
        self.vm.insert_coin('quarter')
        self.vm.insert_coin('quarter')
        self.vm.return_coins()
        self.assertEqual(self.vm.get_returned_coins(),
                         dict(nickel=1, dime=2, quarter=3))

        # "displays ``INSERT COIN''"
        self.assertEqual(self.vm.get_display(),
                         'INSERT COIN')

        return

    def test_sold_out(self):
        #self.vm.refill_product('chips', 50, 5)

        # "when item selected is out of stock, display ``SOLD OUT''"
        self.vm.insert_coin('quarter')
        self.vm.insert_coin('quarter')
        self.vm.select_product('chips')
        self.assertEqual(self.vm.get_display(), 'SOLD OUT')

        # "Check again, amount remaining in the machine"
        self.assertEqual(self.vm.get_display(), '$0.50')

        # "-or- ``INSERT COIN'' when no money in the machine"
        self.vm.return_coins()
        self.vm.select_product('chips')
        self.assertEqual(self.vm.get_display(), 'SOLD OUT')
        self.assertEqual(self.vm.get_display(), 'INSERT COIN')

        return

    #def test_exact_change_only(self):
    #   Not supporting change.

#--#
